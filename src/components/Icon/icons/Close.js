import React from 'react';

import { BaseIcon } from 'components';

export default function Close(props) {
    const { ...rest } = props;

    return (
        <BaseIcon viewBox="0 0 24 24" {...rest}>
            <path
                d="M5.5,5.5 L18.5,18.5"
                stroke="#5700FF"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M18.5,5.5 L5.5,18.5"
                stroke="#5700FF"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </BaseIcon>
    );
}
