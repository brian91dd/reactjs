const initialArticles = {
    articles: [],
    randomText: '',
    isFetching: false,
};

const Articles = (state = initialArticles, action) => {
    switch (action.type) {
    case 'SET_RANDOM_TEXT': {
        const { data } = action.payload;
        return {
            ...state,
            randomText: data.text,
        };
    }
    case 'SET_FETCHING_TEXT': {
        const { data } = action.payload;
        return {
            ...state,
            isFetching: data.isFetching,
        };
    }
    default:
        return state;
    }
};

export default Articles;
