import React from 'react';

import { BaseIcon } from 'components';

export default function Menu(props) {
    const { ...rest } = props;

    return (
        <BaseIcon viewBox="0 0 24 24" {...rest}>
            <g id="Hamburger" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Group-2">
                    <polygon id="Fill-1" fill="#FFFFFF" points="0 24 24 24 24 0 0 0" />
                    <g
                        id="Group-6"
                        transform="translate(3.000000, 6.000000)"
                        stroke="#5700FF"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                    >
                        <path d="M0,1 L18,1" id="Stroke-2" />
                        <path d="M0,11 L18,11" id="Stroke-4" />
                    </g>
                </g>
            </g>
        </BaseIcon>
    );
}
