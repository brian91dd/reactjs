import React from 'react';
import {
    TwitterIcon,
    MediumIcon,
    GithubIcon,
    SlackIcon,
    MeetupIcon,
    GridBox,
    ColBox,
    ContentContainer,
} from 'components';
import styles from './Footer.module.scss';

const Footer = () => {
    return (
        <footer id="footer" className={styles.root}>
            <ContentContainer>
                <GridBox>
                    <ColBox xs={12} md={10} className={styles.content}>
                        <div className={styles.left}>
                            <div className={styles.copyright}>
                                Copyright {new Date().getFullYear()} React Boilerplate
                            </div>
                        </div>
                        <div className={styles.right}>
                            <div className={styles.links}>
                                <a
                                    href="http://localhost:3000/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <GithubIcon />
                                </a>
                                <a href="http://localhost:3000/">
                                    <SlackIcon />
                                </a>
                                <a
                                    href="http://localhost:3000/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <MeetupIcon />
                                </a>
                            </div>
                            <div className={styles.links}>
                                <a
                                    href="http://localhost:3000/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <MediumIcon />
                                </a>
                                <a
                                    href="http://localhost:3000/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    <TwitterIcon />
                                </a>
                            </div>
                        </div>
                    </ColBox>
                </GridBox>
            </ContentContainer>
        </footer>
    );
};

export default Footer;
