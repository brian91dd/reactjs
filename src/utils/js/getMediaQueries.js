import useMedia from 'hooks/useMedia';

export const IsSmall = () => {
    return useMedia(['(max-width: 768px)'], [true], false);
};

export const IsSmallOrMedium = () => {
    return useMedia(['(max-width: 992px)'], [true], false);
};

export const IsMediumOrLarge = () => {
    return useMedia(['(min-width: 769px)'], [true], false);
};
