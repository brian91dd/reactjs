import Axios from 'axios';
import { setRandomText, fetchingRandomText } from 'actions';


export const requestArticle = () => (dispatch) => {
    dispatch(fetchingRandomText({ isFetching: true }));
    return Axios.get('https://jsonplaceholder.typicode.com/todos/1')
        .then((response) => {
            console.log('responseresponse', response);
            dispatch(setRandomText({ text: response.data.title }));
            dispatch(fetchingRandomText({ isFetching: false }));
        });
};
