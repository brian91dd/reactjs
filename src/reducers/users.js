const initialUser = {
    data: {},
    isFetching: false,
    errorLogin: false,
};

const User = (state = initialUser, action) => {
    switch (action.type) {
    case 'SET_USER': {
        const { data } = action.payload;
        return {
            ...state,
            data,
        };
    }
    case 'UNSET_USER': {
        return {
            ...state,
            data: initialUser.data,
        };
    }
    case 'SET_FETCHING_USER': {
        const { data } = action.payload;
        return {
            ...state,
            isFetching: data.isFetching,
        };
    }
    case 'SET_USER_ERROR_LOGIN': {
        const { data } = action.payload;
        return {
            ...state,
            errorLogin: data.errorLogin,
        };
    }
    default:
        return state;
    }
};

export default User;
