import React from 'react';
import PropTypes from 'prop-types';
import { Row } from 'react-flexbox-grid';

const GridBox = ({ gap, children, ...rest }) => {
    const gapsChildren = {
        paddingLeft: gap / 2,
        paddingRight: gap / 2,
    };

    const marginGaps = {
        marginLeft: (gap / 2) * -1,
        marginRight: (gap / 2) * -1,
    };

    const childrenWithProps = React.Children.map(children, child =>
        React.cloneElement(child, { style: gapsChildren })
    );

    return (
        <div {...rest}>
            <Row style={marginGaps}>{childrenWithProps}</Row>
        </div>
    );
};

GridBox.propTypes = {
    children: PropTypes.node.isRequired,
    gap: PropTypes.number,
};

GridBox.defaultProps = {
    gap: 40,
};

export default GridBox;
