import React from 'react';
import { ContentContainer, Logo } from 'components';

const Header = ({ actionLogOut }) => {
    let userDataStored;

    try {
        userDataStored = JSON.parse(localStorage.getItem('user'));
    } catch (error) {
        userDataStored = null;
    }

    return (
        <ContentContainer>
            <Logo img="logo" />
        </ContentContainer>
    );
};

export default Header;
