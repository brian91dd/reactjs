import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { getThemeLabel } from '../../utils/js';

import styles from './Button.module.scss';

const Button = ({
    theme, text, url, ...rest
}) => (
    <a className={classnames(styles.root, styles[getThemeLabel(theme)])} href={url} {...rest}>
        <div className={classnames(styles.textContainer)}>{text}</div>
    </a>
);

export const buttonPropTypes = {
    theme: PropTypes.oneOf(['Fill', 'Stroke']),
    text: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
};

Button.propTypes = buttonPropTypes;

Button.defaultProps = {
    theme: 'Fill',
};

export default Button;
