export const setRandomText = data => ({ type: 'SET_RANDOM_TEXT', payload: { data } });
export const getRandomText = () => ({ type: 'GET_RANDOM_TEXT' });
export const fetchingRandomText = data => ({ type: 'SET_FETCHING_TEXT', payload: { data } });
