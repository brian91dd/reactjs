const windowMock = {
    matchMedia: () => true,
    localStorage: {
        getItem: () => true,
        setItem: () => true,
    },
};

export default windowMock;
