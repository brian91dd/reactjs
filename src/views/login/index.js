/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUser } from 'actions';
import { loginUser } from 'thunks';
import { GridBox, ColBox, ContentContainer } from 'components';
import styles from './login.module.scss';


const Login = ({ loginUser, errorLogin, userData }) => {
    const [userName, setUserName] = useState('');
    const [userPassword, setUserPassword] = useState('');
    const setLogin = () => {
        console.log('userPassword', userPassword);

        loginUser({ name: userName, password: userPassword });
    };

    if (userData && userData.user) return <Redirect to="/home-user" />;

    return (
        <ContentContainer>
            <GridBox>
                <ColBox xs={4} className={styles.container}>
                    <div>
                        <h1 className={styles.header}>Welcome!</h1>
                        <Link to="/register" className={styles.submit}>Create Account</Link>
                        <div className={styles.formItem}>
                            <input name="username" type="text" placeholder="Username" onKeyUp={e => setUserName(e.target.value)} />
                        </div>
                        <div className={styles.formItem}>
                            <input type="password" placeholder="Password" onKeyUp={e => setUserPassword(e.target.value)} />
                        </div>
                        { errorLogin && <div className={styles.error}>User or password incorrect</div> }
                        <a href="javascript:void(0)" className={styles.submit} onClick={() => setLogin()}>Log in</a>
                    </div>
                </ColBox>
            </GridBox>
        </ContentContainer>
    );
};

const mapStateToProps = state => ({
    userData: state.users.data,
    errorLogin: state.users.errorLogin,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    setUser,
    loginUser,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Login);
