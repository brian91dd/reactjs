FROM node:10.15.1-alpine

ARG CRW_APP_FOLDER=./
WORKDIR /usr/app

COPY package.json .
RUN npm install

COPY $CRW_APP_FOLDER .

CMD ["npm", "start"]