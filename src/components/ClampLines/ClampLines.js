import LinesEllipsis from 'react-lines-ellipsis';
import responsiveHOC from 'react-lines-ellipsis/lib/responsiveHOC';

const ClampLines = responsiveHOC()(LinesEllipsis);
export default ClampLines;
