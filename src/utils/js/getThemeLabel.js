export const getThemeLabel = string => `${string.toLowerCase()}Theme`;

export default getThemeLabel;
