import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Logo.module.scss';

const Logo = ({ img, ...rest }) => (
    <Link to="/" {...rest} className={styles.logo}>
        <img src={img} alt="Forctis" />
    </Link>
);

Logo.propTypes = {
    img: PropTypes.string,
};

export default Logo;
