import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';

import styles from './Base.module.scss';

const cx = classNames.bind(styles);

export default function Base(props) {
    const { height, width, children, ...rest } = props;

    // We want the default dimensions to be 100%, which covers most
    // use-cases, but only if BOTH the width and height props are unset
    const trueWidth = width || height ? width : '100%';
    const trueHeight = width || height ? height : '100%';

    return (
        <svg
            width={trueWidth}
            height={trueHeight}
            focusable="false"
            className={cx('base')}
            {...rest}
        >
            {children}
        </svg>
    );
}

Base.defaultProps = {
    height: null,
    width: null,
};

Base.propTypes = {
    height: PropTypes.string,
    width: PropTypes.string,
    children: PropTypes.node,
};
