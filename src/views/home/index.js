import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setRandomText } from 'actions';
import { requestArticle } from 'thunks';
import styles from './home.module.css';


const Home = ({ articleRandomText, setRandomText, requestArticle }) => {
    useEffect(() => {
        setRandomText({ text: 'fafafa' });
    }, []);

    const [name, setName] = useState('name 1');

    const changeText = () => {
        requestArticle();
    };

    return (
        <div>
            <h1 className={styles.errorComponent}>Hola</h1>
            <p>{articleRandomText}</p>
            <p>{name}</p>
            <button onClick={() => setName('name 2')}>Set Name</button>
            <button onClick={() => changeText()}>Fetch text</button>
            <Link to='/test'>Go to test</Link>
        </div>
    );
};

const mapStateToProps = state => ({
    articleRandomText: state.articles.randomText,
});

const mapDispatchToProps = dispatch => bindActionCreators({
    setRandomText,
    requestArticle,
}, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Home);
