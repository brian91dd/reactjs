import Axios from 'axios';
import {
    setUser, fetchingUser, unsetUser, setUserErrorLogin,
} from 'actions';

Axios.defaults.headers.post['Content-Type'] = 'application/json';

export const loginUser = payload => (dispatch) => {
    const { name, password } = payload;
    dispatch(fetchingUser({ isFetching: true }));
    return Axios.post('http://localhost:3000/api/user/login', {
        name,
        password,
    })
        .then(() => {
            localStorage.setItem('user', JSON.stringify({ user: payload.name }));
            dispatch(setUser({ user: payload.name }));
            dispatch(fetchingUser({ isFetching: false }));
        })
        .catch(() => {
            dispatch(setUserErrorLogin({ errorLogin: true }));
            dispatch(fetchingUser({ isFetching: false }));
        });
};

export const registerUser = payload => () => {
    const { password, email, name } = payload;

    return Axios.post('http://localhost:3000/api/user/register', {
        password,
        email,
        name,
    })
        .then((response) => {
            console.log(response);
        })
        .catch((err) => {
            console.log(err);
        });
};

export const logoutUser = () => (dispatch) => {
    dispatch(fetchingUser({ isFetching: true }));
    localStorage.removeItem('user');
    dispatch(unsetUser());
    dispatch(fetchingUser({ isFetching: false }));
};
