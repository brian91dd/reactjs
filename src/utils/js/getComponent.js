import React from 'react';
import * as components from '../../components';

export const getComponent = module => {
    const { id, internal, ...rest } = module;

    const componentsMap = {
        ContentfulComponentBio: components.Bio,
        ContentfulComponentBullet: components.Bullet,
        ContentfulComponentButton: components.Button,
        ContentfulComponentCard: components.Card,
        ContentfulComponentEmailLink: components.EmailLink,
        ContentfulComponentQuote: components.Quote,
    };

    if (!internal) {
        return null;
    }

    const Component = componentsMap[internal.type];
    if (Component) {
        return <Component key={id} {...rest} />;
    }
    return null;
};

export default getComponent;
