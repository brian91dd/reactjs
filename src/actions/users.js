export const setUser = data => ({ type: 'SET_USER', payload: { data } });
export const setUserErrorLogin = data => ({ type: 'SET_USER_ERROR_LOGIN', payload: { data } });
export const getUser = () => ({ type: 'GET_USER' });
export const fetchingUser = data => ({ type: 'SET_FETCHING_USER', payload: { data } });
export const unsetUser = data => ({ type: 'UNSET_USER', payload: { data } });
