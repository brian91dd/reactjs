import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames/bind';

import styles from './Nav.module.scss';

const cx = classNames.bind(styles);

const Nav = ({ current, items, ...rest }) => (
    <ul {...rest} className={cx('root', rest.className)}>
        {items.map(item => (
            <li
                key={item.slug}
                className={cx('item', {
                    active: item.slug === current,
                })}
            >
                <Link to={item.slug}>{item.title}</Link>
            </li>
        ))}
    </ul>
);

Nav.propTypes = {
    current: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            slug: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
        }),
    ).isRequired,
};

export default Nav;
