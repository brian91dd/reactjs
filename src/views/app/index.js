import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Nav, ContentContainer } from '../../components';
import Home from '../home';
import Test from '../test';
import Login from '../login';

const App = () => (
    <ContentContainer>
        <Nav
            current="home"
            items={[
                {
                    title: 'Home',
                    slug: 'home',
                }, {
                    title: 'About us',
                    slug: 'about-us',
                },
            ]}
        />
        <Switch>
            <Route
                exact
                path="/"
                component={Home}
            />
            <Route
                exact
                path="/test"
                component={Test}
            />
            <Route
                exact
                path="/login"
                component={Login}
            />
        </Switch>
        <Footer />
    </ContentContainer>
);

export default App;
