import moment from 'moment';

const getFormatDate = (date, format) => moment(new Date(date)).format(format);
export default getFormatDate;
