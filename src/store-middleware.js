import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Articles, Users } from 'reducers';
import thunk from 'redux-thunk';

const logger = store => next => (action) => {
    console.log('will dispatch', action);
    const actionResult = next(action);
    console.log('state after dispatch', store.getState());
    // Este seguramente sera la acción, excepto
    // que un middleware anterior la haya modificado.
    return actionResult;
};

const crashReporter = store => next => (action) => {
    try {
        return next(action);
    } catch (err) {
        console.error('Caught an exception!', err, store.getState());
        throw err;
    }
};

const combinedReducers = combineReducers({
    articles: Articles,
    users: Users,
});

const store = createStore(
    combinedReducers,
    // applyMiddleware() tells createStore() how to handle middleware
    applyMiddleware(logger, crashReporter, thunk),
);

export default store;
