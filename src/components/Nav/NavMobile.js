import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import { Link } from 'react-dom';
import { ContentContainer } from 'components';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

import styles from './NavMobile.module.scss';

const cx = classNames.bind(styles);

const NavMobile = ({ current, items, setVisibility, isVisible, ...rest }) => {
    if (isVisible) {
        disableBodyScroll();
    } else {
        enableBodyScroll();
    }

    const handleClick = () => {
        setVisibility(false);
    };

    return (
        <Fragment>
            <div className={cx('mobileMenu', isVisible ? 'visible' : '')} {...rest}>
                <ContentContainer>
                    <ul className={cx('items')}>
                        <li
                            className={cx('item', {
                                active: current === 'home',
                            })}
                        >
                            <Link to="/" onClick={handleClick}>
                                Home
                            </Link>
                        </li>
                        {items.map(item => (
                            <li
                                key={item.slug}
                                className={cx('item', {
                                    active: current === item.slug,
                                })}
                            >
                                <Link to={item.slug} onClick={handleClick}>
                                    {item.title}
                                </Link>
                            </li>
                        ))}
                        <li className={cx('item')}>
                            <a href="#contact" onClick={handleClick}>
                                Contact
                            </a>
                        </li>
                    </ul>
                </ContentContainer>
            </div>
            <div className={cx('overlay', { visible: isVisible })} />
        </Fragment>
    );
};

NavMobile.defaultProps = {
    setVisibility: () => true,
};

NavMobile.propTypes = {
    setVisibility: PropTypes.func,
    isVisible: PropTypes.bool,
    current: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(
        PropTypes.shape({
            slug: PropTypes.string.isRequired,
            title: PropTypes.string.isRequired,
        })
    ).isRequired,
};

NavMobile.defaultProps = {
    isVisible: false,
};

export default NavMobile;
