import PropTypes from 'prop-types';
import React from 'react';
import classnames from 'classnames';

import styles from './ContentContainer.module.scss';

const ContentContainer = ({
    theme, className, children, ...rest
}) => (
    <div
        className={classnames(
            styles.root,
            {
                [styles.theme]: theme,
            },
            className,
        )}
        {...rest}
    >
        <div className={styles.inner}>{children}</div>
    </div>
);

ContentContainer.propTypes = {
    flush: PropTypes.bool,
    className: PropTypes.string,
    children: PropTypes.node,
};

export default ContentContainer;
