import { useEffect, useState } from 'react';
import { windowMock } from 'utils/js';

// https://stackoverflow.com/questions/55840294/how-do-i-fix-missing-dependency-in-react-hook-useeffect

export default function useMedia(queries, values, defaultValue) {
    const w = typeof window === 'undefined' ? windowMock : window;
    const mediaQueryLists = queries.map(q => w.matchMedia(q));

    const getValue = () =>

    {
        const index = mediaQueryLists.findIndex(mql => mql.matches);
        return typeof values[index] !== 'undefined' ? values[index] : defaultValue;
    };

    const [value, setValue] = useState(getValue);

    useEffect(() => {
        const handler = () => setValue(getValue);
        mediaQueryLists.forEach(mql => mql.addListener(handler));
        return () => mediaQueryLists.forEach(mql => mql.removeListener(handler));
    }, [getValue, mediaQueryLists]);

    // eslint-disable-next-line consistent-return
    return value;
}
